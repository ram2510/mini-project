
//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

//this variable will store the start url
// api = "http://localhost:3000/"
api = "https://codemmunity.herokuapp.com/"

//this function will be used to perform various function in the browser url
var Nav = /** @class */ (function() {
    function Nav() {}
    Nav.assign = function(url) {
        window.location.assign(url);
    };
    Nav.replace = function(url) {
        window.location.replace(url);
    };
    Nav.open = function(url) {
        window.open(url, '_blank', 'location=no');
    };
    Nav.close = function() {
        window.close();
    };
    return Nav;
}());

function searchName(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].name.indexOf(nameKey)!=-1) {
            return myArray[i];
        }
    }
}

function searchP(){
    var search = document.getElementById('nameSer')
        $.ajax({
            url:api+"userAll",
            method:"post",
            success:function(data){
                $('#people').html("");
                var ser = $('#nameSer').val()
                var users = data;
                var resultObject = searchName(ser, users);
                var data1 = JSON.stringify(resultObject);
                setData('fri',data1);
                // console.log(getData('fri'));
                // console.log(resultObject);
                $('#people').append(`<a href="${api}view/profView.html" class="w3-bar-item w3-button ">${resultObject.name} (${resultObject.job})</a>`)
            }
        }) 
}

//Storage Functions
function setData(cname, cvalue) {
    window.localStorage.setItem(cname, JSON.stringify(cvalue));
}

function getData(cname) {
    return JSON.parse(JSON.parse(window.localStorage.getItem(cname)));
}

function checkData(cname) {
    var user = getData(cname);
    if (user != null) {
        return true;
    } else {
        return false;
    }
}

function deleteData(cname) {
    window.localStorage.removeItem(cname);
}



function changeLang(){
    // console.log(editAreaLoader.getValue('textarea_1'));
    $('#codeEdit').remove()
    $('#view').append(`<script id="codeEdit" language="javascript" type="text/javascript">
    editAreaLoader.init({
        id : "textarea_1"		// textarea id
        ,syntax: "${$('#lang').val()}"			// syntax to be uses for highgliting
        ,allow_toggle:false
        ,start_highlight: true

    });
    </script>`)
    
}


     
function prof(obj){
   return `<div id="id01" class="w3-modal" >
   <div class="w3-modal-content w3-animate-opacity w3-card-4">
     <header class="w3-container w3-deep-orange"> 
       <span onclick="document.getElementById('id01').style.display='none'"
       class="w3-button w3-display-topright">&times;</span>
       <h2>Comments</h2>
     </header>
     <div class="w3-container cont">    
     </div>
     <footer class="w3-container">
     <br>
     <div id="comInp">
     </div>
     </footer>
   </div>
 </div> 
   <div class="w3-top">
       <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
       <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
       <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-d4"><i class="fa fa-home w3-margin-right"></i>Logo</a>
       <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="News"><i class="fa fa-globe"></i></a>
       <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" title="Account Settings"><i class="fa fa-user"></i></a>
       <a href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" onclick="createRoom()" title="Messages"><i class="fa fa-envelope"></i></a>
       <a href="#" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="My Account">
           <!-- <img src="/w3images/avatar2.png" class="w3-circle" style="height:23px;width:23px" alt="Avatar"> -->
       </a>
   </div>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
   <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
   <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
   <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
   <a href="#" class="w3-bar-item w3-button w3-padding-large">My Profile</a>
</div>

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top:80px">
   <!-- The Grid -->
   <div class="w3-row">
       <!-- Left Column -->
       <div class="w3-col m3">
           <!-- Profile -->
           
           <!-- Interests -->
           <div class="w3-card w3-round w3-white w3-hide-small">
               <div class="w3-container w3-padding">
                   <p> ${obj.name}'s Interests</p>
                   <div id="intrest">
                   </div>
               </div>
           </div>
           <br>
           <!-- End Left Column -->
       </div>

       <!-- Middle Column -->
       <div class="w3-col m7">
           <div class='w3-center' style="margin-left:10%;">
          <div class="w3-panel w3-white w3-card w3-container" style="max-width: 90%;">
              <h2>Coders Profile</h2>
          </div>
      </div>
      <br>
      <div class="w3-row-padding">
              <div class="w3-col m12">
                  <div class="w3-card w3-round w3-white">
                      <div class="w3-container w3-padding">
                         <h6 class="w3-opacity" id="cont">Name</h6>
                         <h3>${obj.name}</h3>   
                         <h6 class="w3-opacity">Job</h6>
                          <h3>${obj.job}</h3>    
                      </div>
                  </div>
              </div>
          </div>
          <br>
          <div class='w3-center' style="margin-left:10%;">
                  <div class="w3-panel w3-white w3-card w3-container" style="max-width: 90%;">
                      <h2>Posts By coder</h2>
                  </div>
              </div>
              <div id="post">   
              </div>
           
           <!-- End Middle Column -->
       </div>

       <!-- Right Column -->
       <div class="w3-col m2">
           <div class="w3-card w3-round w3-white w3-center">
               <div class="w3-container w3-padding">
                   <p>Upcoming Events:</p>
                   <!-- <img src="/w3images/forest.jpg" alt="Forest" style="width:100%;"> -->
                   <p><strong>Hackathon</strong></p>
                   <p>Friday 15:00</p>
                   <p><button class="w3-button w3-block w3-theme-l4 w3-padding">Info</button></p>
               </div>
           </div>
           <br>        
           <br>
           <div class="w3-panel w3-round w3-white w3-center w3-margin w3-card-2">
              <input type="search" class="w3-input w3-margin-top" placeholder="Enter Name" id="nameSer">
              <br>
              <div class="w3-bar-block" id="people">
                     
              </div>
          </div>
<!-- End Right Column -->
       <!-- End Grid -->
   </div>

   <!-- End Page Container -->
</div>
</div>
<br>
 <div class="cbody">
   <div id="live-chat">
       <header class="clearfix">
           <a href="#" class="chat-close" onclick="document.querySelector('.cbody').style.display='none'">x</a>
           <h4>${getData('fri').name}</h4>
           <span class="chat-message-counter">3</span>
       </header>
       <div class="chat">
           <div class="chat-history">
               <div class="chat-message clearfix">
               </div>
               <input type="text" class="w3-bottom" id="textEnter" placeholder="Type your message Press enter to send it" autofocus on>
           </div>
       </div>
   </div>
</div> 
 <script>
 document.getElementById('nameSer').addEventListener("keyup",function(event){
    if(event.keyCode===13){
        searchP();
    }
})
document.querySelector('.cbody').style.display="none";
document.getElementById('textEnter').addEventListener('keyup',function(event){
    if(event.keyCode===13){
        sendMessage();
    }
})
document.querySelector('.cbody').style.display="none";

</script> `
};














/// the main.js docstring
function main(obj){
    return ` 
    <div id="id01" class="w3-modal" >
                <div class="w3-modal-content w3-animate-opacity w3-card-4">
                  <header class="w3-container w3-deep-orange"> 
                    <span onclick="document.getElementById('id01').style.display='none'"
                    class="w3-button w3-display-topright">&times;</span>
                    <h2>Comments</h2>
                  </header>
                  <div class="w3-container cont">
                    <p>Some text..</p>
                    <br>
                    <hr>
                    <p>Some text..</p>
                    <br>
                    <hr>
                  </div>
                  <footer class="w3-container">
                  <br>
                  <div id="comInp">
                  </div>
                  </footer>
                </div>
              </div>
    <div class="w3-top">
    <div class="w3-bar w3-theme-d2 w3-left-align w3-large">
        <a class="w3-bar-item w3-button w3-hide-medium w3-hide-large w3-right w3-padding-large w3-hover-white w3-large w3-theme-d2" href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars"></i></a>
        <a href="main.html" class="w3-bar-item w3-button w3-padding-large w3-theme-d4"><i class="fa fa-home w3-margin-right"></i>Codemunnity</a>
        <div class="w3-dropdown-hover w3-hide-small">
            <button class="w3-button w3-padding-large notif" title="Notifications"><i class="fa fa-bell"></i><span class="w3-badge w3-right w3-small w3-green" id="notifBadge">3</span></button>
            <div class="w3-dropdown-content w3-card-4 w3-bar-block" id="notif" style="width:300px;height:300px;overflow-y:auto">    
            </div>
         </div>        
        <div href="#" class="w3-bar-item w3-button w3-hide-small w3-padding-large w3-hover-white" onclick="Nav.assign('../index.html');deleteData('user')" title="Account Settings"><i class="fa fa-user"></i></div>
        <div class="w3-dropdown-hover w3-hide-small">
            <button class="w3-button w3-padding-large messNotif1"  title="Message Notifications"><i class="fa fa-envelope"></i><span class="w3-badge w3-right w3-small w3-green" id="messNotifBadge">0</span></button>
            <div class="w3-dropdown-content w3-card-4 w3-bar-block" id="messNotif" style="width:300px;height:300px;overflow-y:auto">   
            </div>
        </div>
        <a href="#" class="w3-bar-item w3-button w3-hide-small w3-right w3-padding-large w3-hover-white" title="My Account">
            <!-- <img src="/w3images/avatar2.png" class="w3-circle" style="height:23px;width:23px" alt="Avatar"> -->
        </a>
    </div>
</div>

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 1</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 2</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Link 3</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">My Profile</a>
</div>

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top:80px">
    <!-- The Grid -->
    <div class="w3-row">
        <!-- Left Column -->
        <div class="w3-col m3">
            <!-- Profile -->
            <div class="w3-card w3-round w3-white">
                <div class="w3-container">
                    <h4 class="w3-center">My Profile</h4>
                    <p class="w3-center">
                        <!-- <img src="/w3images/avatar3.png" class="w3-circle" style="height:106px;width:106px" alt="Avatar">--></p>
                    <hr>
                    <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme w3-padding"></i>${obj.job}</p>
                    <hr>
                    <p><i class="fa fa-pencil fa-fw w3-margin-right w3-text-theme w3-padding"></i>${obj.name}</p>
                </div>
            </div>
            <br>

            <!-- Accordion -->
            <div class="w3-card w3-round">
                <div class="w3-white">
                    <button onclick="myFunction('Demo1')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-file-text fa-fw w3-margin-right"></i> My Posts</button>
                    <div id="Demo1" class="w3-hide w3-container">
                        <p>${obj.noOfPosts}</p>
                    </div>
                    <button onclick="myFunction('Demo2')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-handshake-o fa-fw w3-margin-right"></i>CoderBuddys</button>
                    <div id="Demo2" class="w3-hide w3-container">
                        <p>${obj.CB}</p>
                    </div>
                </div>
            </div>
            <br>

            <!-- Interests -->
            <div class="w3-card w3-round w3-white w3-hide-small">
                <div class="w3-container w3-padding">
                    <p>Interests</p>
                    <p id="intrest">
                        <script>
                        
                        </script>
                    </p>
                </div>
            </div>
            <br>
            <!-- End Left Column -->
        </div>

        <!-- Middle Column -->
        <div class="w3-col m7">

            <div class="w3-row-padding">
                <div class="w3-col m12">
                    <div class="w3-card w3-round w3-white">
                        <div class="w3-container w3-padding">
                           <h6 class="w3-opacity" >Things you wish to share</h6>
                           <p contenteditable="true" class="w3-border w3-padding" id="cont"></p>   
                           <h6 class="w3-opacity">Language</h6>
                           <select class="w3-select" name="option" onchange="changeLang()" id="lang">
                                   <option value="" disabled selected>Choose your option</option>
                                   <option value="c">C</option>
                                   <option value="js">javascript</option>
                                   <option value="rust">Rust</option>
                                   <option value="java">Java</option>
                                   <option value="cpp">C++</option>
                                   <option value="perl">perl</option>
                                   <option value="python">python</option>
                                   <option value="basic">Basic</option>
                                   <option value="css">CSS</option>
                                   <option value="html">html</option>
                                   <option value="php">php</option>
                                   <option value="ruby">Ruby</option>
                                   <option value="sql">SQL</option>
                                   <option value="vb">Visual Basic</option>
                                 </select>                                                    
                            <h6 class="w3-opacity">Code</h6>
<textarea id="textarea_1" name="content" cols="80"></textarea>
                           <button type="button" class="w3-button w3-theme w3-margin-top" id="Wpost"><i class="fa fa-pencil"></i> Post</button>
                        </div>
                    </div>
                </div>
            </div>
    <div id="post">   
    </div>
    <!-- End Middle Column -->
    </div>
        <!-- Right Column -->
        <div class="w3-col m2">
            <div class="w3-card w3-round w3-white w3-center">
                <div class="w3-container w3-padding">
                    <p>Upcoming Events:</p>
                    <!-- <img src="/w3images/forest.jpg" alt="Forest" style="width:100%;"> -->
                    <p><strong>Hackathon</strong></p>
                    <p>Friday 15:00</p>
                    <p><button class="w3-button w3-block w3-theme-l4 w3-padding">Info</button></p>
                </div>
            </div>
            <br>
            
            <br>
            <div class="w3-panel w3-round w3-white w3-center w3-margin w3-card-2">
               <input type="search" class="w3-input w3-margin-top" placeholder="Enter Name" id="nameSer">
               <br>
               <div class="w3-bar-block" id="people">
               <ul class="w3-ul w3-left-align">
    <li class="w3-bar">    
      <div class="w3-bar-item">
        <span class="">Mike</span><br>
        <span>Web Designer</span>
      </div>
    </li>

  </ul>
                </div>
           </div>
<!-- End Right Column -->
        <!-- End Grid -->
    </div>

    <!-- End Page Container -->
</div>
<br>
<script>

    document.getElementById('nameSer').addEventListener("keyup",function(event){
        if(event.keyCode===13){
            searchP();
        }
    })

$('#Wpost').click(function(){
    // console.log(document.getElementById('codeShare').textContent);
    // console.log(document.getElementById('cont').textContent);

    if((document.getElementById('cont').textContent) && ($('#lang').val()) && (editAreaLoader.getValue('textarea_1'))){
        var formData = {
            email:getData('user')[2].email,
            name:getData('user')[2].name,
            code:editAreaLoader.getValue('textarea_1'),
            lang:$('#lang').val(),
            cont:document.getElementById('cont').textContent
        }
        $.ajax({
            url:api+'postAdd',  
            method:'post',
            data:formData,
            success:function(data){
                console.log(data);
                Nav.replace('/view/main.html');
            },
            error:function(jqXHR,exception){
                if (jqXHR.status===500){
                    $.blockUI({ 
                        message: '<h3>Our server is experiencing issue</h3>', 
                        timeout: 2000 
                    }); 
                }
            }
        })
    }else{
        $.blockUI({ 
            message: '<h3>please Enter all the details</h3>', 
            timeout: 2000 
        });
    }
})


</script>
      

`
}



function createRoom(){
    var email = getData('fri').email;
    var name = getData('fri').name;
    data = {
        messageFromEmail:getData('user')[2].email,
        messageToEmail:email,
    }
    $.ajax({
        url:api+'messFet',
        method:'post',
        data:data,
        success:function(data){
            socket.emit('room',{
                messageToEmail:email,
                messageToName:name,
                messageFromName:getData('user')[2].name,
                messageFromEmail:getData('user')[2].email
            },(data)=>{
                console.log(data);
        
            });
            for(var i=0;i<data.length;i++){
                
            $('.chat-message').append(`
            <div class="chat-message-content clearfix w3-margin-bottom">
						
            <span class="chat-time">${moment(data[i].createdAt).format('h:mm a')}</span>

            <h4>${data[i].messageFromName}</h4>

            <p>${data[i].messageValue}</p>
            <hr>
        </div>
            `)
            }
        }
    })

    document.querySelector('.cbody').style.display="block";
    
   
}
        
function sendMessage(){
    var friendId = getData('fri')._id;
    var email = getData('fri').email;
    var name = getData('fri').name;
    socket.emit('sendMessage',{
        messageToEmail:email,
        messageToName:name,
        messageFromName:getData('user')[2].name,
        messageFromEmail:getData('user')[2].email,
        messageValue:document.getElementById('textEnter').value,
        createdAt:moment().valueOf()
    },(data)=>{
        $('#textEnter').val('');
    })
}

function myFunction(id) {
    //this function will be used to close the chat window when clicked
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
        x.previousElementSibling.className += " w3-theme-d1";
    } else {
        x.className = x.className.replace("w3-show", "");
        x.previousElementSibling.className =
            x.previousElementSibling.className.replace(" w3-theme-d1", "");
    }
}
