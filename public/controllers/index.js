//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

//this fucntion will run once a ajax call stops

$(document).ready(function(){
    if(getData('user')){
        Nav.replace('/view/main.html');
    }
    //this will run whenever a user enters a url for one sec
    $.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
    setTimeout($.unblockUI, 1000); 
})

$('#sub').on('click',function(e){
    document.getElementById('pass').type="password"
    //this function will run once a user clicks on the login button
    e.preventDefault();
    if($('#log').val() && document.getElementById('pass').value){
    var email = $('#log').val();
    //we take the file from the file upload button
    $.ajax({
        url:api+"userFet",
        method:'post',
        data:{
            email:document.getElementById('log').value,
            password:document.getElementById('pass').value
        },
        success:function(data){
            console.log(data);
            if(data[0]===200){
            var data1 = JSON.stringify(data);
            setData('user',data1);
            Nav.replace(api+"view/main.html");
            }else{
                $.blockUI({ 
                    message: '<h3>No User detected</h3>', 
                    timeout: 2000 
                }); 
            }

        },
        error:function(){
            $.blockUI({ 
                message: '<h3>Please enter correct details</h3>', 
                timeout: 2000 
            }); 
        }
    })
}else{
    $.blockUI({ 
        message: '<h3>Please Enter Every detail</h3>', 
        timeout: 2000 
    }); 
}
})
var passMatch = false;

function checkPass(){
   var pass = $('#regPass').val();
   var repass = $('#regRepass').val();
   console.log(pass);
   console.log(repass);
    if (pass==repass){
        $('#passCheck').html('Password Match!');
        passMatch=true;
    }else{
        $('#passCheck').html('Password Do Not Match!'); 
    }
}

$('#regSub').on('click',function(e){
    document.getElementById('pass').type="password"
    e.preventDefault();
    if (($('#regName').val()) && ($('#regEmail').val()) && ($('#regPass').val()) && ($('#regJob').val()) ){
    if(passMatch){
        // var tmppath = URL.createObjectURL($('#file-upload').get(0).files[0]);    
        // console.log(tmppath);   
        //this function will run once a user clicks on the register button
        //and the variable pass match is true
        //this is the formData
        // var form = document.forms[0];
        // var formData = new FormData(form);
        formData = {
            name:$('#regName').val(),
            email:$('#regEmail').val(),
            password:$('#regPass').val(),
            job:$('#regJob').val(),
        }
        console.log(formData);
        $.ajax({
            url:api+'user',
            method:'post',
            data:formData,
            success:function(data){
                Nav.replace('/');
                // console.log(data);
            },
            error:function(jqXHR, exception){
                if(jqXHR.status === 500){
                    $.blockUI({ 
                        message: '<h3>Internal Server Error</h3>', 
                        timeout: 2000 
                    })
                }
                if(jqXHR.status===401){
                    $.blockUI({ 
                        message: '<h3>Email Already Exists</h3>', 
                        timeout: 2000 
                    })
                }
                if(jqXHR.status===400){
                    $.blockUI({ 
                        message: '<h3>Please Enter Correct Details</h3>', 
                        timeout: 2000 
                    })
                }
            }
        })
        

    }else{
        //this will display a block 
        $.blockUI({ 
            message: '<h3>Passwords Do not Match</h3>', 
            timeout: 2000 
        }); 
    }
}else{
    //this will display a block 
    $.blockUI({ 
        message: '<h3>Please Enter Every detail</h3>', 
        timeout: 2000 
    }); 
}
})

$('#passEye').on('click',function(){
    // if($('passEye').prop('type'))
    // console.log($('#passEye').attr('type'));
    // console.log(document.getElementById('regPass').type);
    if(document.getElementById('regPass').type==="password"){
        document.getElementById('regPass').type="text";
    }else{
        document.getElementById('regPass').type="password"
    }
})
$('#rePassEye').on('click',function(){
    // if($('passEye').prop('type'))
    // console.log($('#passEye').attr('type'));
    // console.log(document.getElementById('regPass').type);
    if(document.getElementById('regRepass').type==="password"){
        document.getElementById('regRepass').type="text";
    }else{
        document.getElementById('regRepass').type="password"
    }
})
$('#logEye').on('click',function(){
    
    // if($('passEye').prop('type'))
    // console.log($('#passEye').attr('type'));
    // console.log(document.getElementById('regPass').type);
    if(document.getElementById('pass').type==="password"){
        document.getElementById('pass').type="text";
    }else{
        document.getElementById('pass').type="password"
    }
})