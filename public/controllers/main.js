var socket = io();


$(document).ready(function(){
    if(getData('fri'!=undefined)){
        deleteData('fri');
    }
    //this will run whenever a user enters a url for one sec
    $.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
 
    $.ajax({
        url:api+"userFetRef",
        method:'post',
        data:{
            email:getData('user')[2].email,
        },
        success:function(data){
            if(data[0]===200){
            var data1 = JSON.stringify(data);
            setData('user',data1);
            $('#view').append(main(data[2]));
            }else{
                $.blockUI({ 
                    message: '<h3>No User detected</h3>', 
                    timeout: 2000 
                }); 
            }

        },
        error:function(){
            $.blockUI({ 
                message: '<h3>Please enter correct details</h3>', 
                timeout: 2000 
            }); 
        }
    })
    var user = getData('user')[2];
    if(user===undefined){
        Nav.replace('/');
    } 
    $.ajax({
        url:api+'notifFet',
        method:'post',
        data:{
            email:getData('user')[2].email
        },
        success:function(data){
            var count=0;
            for(var i=0;i<data.length;i++){
                if(data[i].type==="like"){
                    $('#notif').append(`<div class="w3-bar-item w3-button">${data[i].notifBy} (${data[i].notifByJob}) : Liked Your post</div>
                    <hr>
                    `)
                    if(data[i].status===0){
                        count++;
                    }
                }else{
                    $('#notif').append(`<div class="w3-bar-item w3-button">${data[i].notifBy} (${data[i].notifByJob}) : Commented on Your post</div>
                    <hr>
                    `)

                    if(data[i].status===0){
                        count++;
                    }
                }
            }
            $('#notifBadge').remove();
            $('button.notif').append(`<span class="w3-badge w3-right w3-small w3-green" id="notifBadge">${count}</span>`)
        },
        error:function (jqXHR){
            if (jqXHR.status===500){
                $.blockUI({ 
                    message: '<h3>Our server is experiencing issue</h3>', 
                    timeout: 2000 
                }); 
            }
        }
    })  
    setTimeout($.unblockUI, 2000); 
    $.ajax({
        url:api+'messNot',
        method:'post',
        data:{
            userName:getData('user')[2].email
        },
        success:function(data){
            console.log(data);
            appendNotif(data);
            },error:function(jqXHR,exception){
            alert('hello');
        }
    })
    $.ajax({
        url:api+'post',
        method:'post',
        success:function(data){
            for (var i=0;i<data.length;i++){
                $('#post').append(`<div class="w3-container w3-card w3-white w3-hover-shadow w3-round w3-margin"><br>
                <!-- <img src="/w3images/avatar5.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:60px"> -->
                <span class="w3-right w3-opacity">${moment(data[i].time).format("dddd, MMMM Do YYYY, h:mm a")}</span>
                <h4>${data[i].postName}</h4><br>
                <hr class="w3-clear">
                <div class="w3-padding">
${data[i].postCont}                   </div>
               <div class="w3-padding">
                   Language:${data[i].postLang}
               </div>
               <textarea id="codeArea_${i}" name="content" cols="80">
${data[i].postCode}        
       </textarea>   
            <div id="lik" class="lik">                                    
               <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom like" onclick="sendLikeData(this.id)" id="${data[i]._id}"><i class="fa fa-thumbs-up" id="${data[i]._id}likLog"></i>  Like <span class="w3-badge likeSpan" style="margin-left:10px;" id="${data[i]._id}likSpan">${data[i].likes.length}</span></button>
               <button type="button" class="w3-button w3-theme-d2 w3-margin-bottom comm" onclick="sendCommData(this.id)" id="${data[i]._id}"><i class="fa fa-comment" id="${data[i]._id}comLog"></i>  Comment <span class="w3-badge commSpan" style="margin-left:10px;" id="${data[i]._id}comSpan">${data[i].comments.length}</span></button>
            </div>
        </div>
        
`)
for(j=0;j<data[i].likes.length;j++){
    if(getData('user')[2].name===data[i].likes[j]){
        $(`#${data[i]._id} #${data[i]._id}likLog`).addClass('w3-text-black');
    }
}
            }
for (var i=0;i<data.length;i++){
    $('#post').append(`<script id="codeEdit" language="javascript" type="text/javascript">
    editAreaLoader.init({
        id : "codeArea_${i}"		// textarea id
        ,syntax: "${data[i].postLang}"			// syntax to be uses for highgliting
        ,allow_toggle:false
        ,is_editable:false
        ,start_highlight: true
    });		
    </script>`)
}
            

        },
        error:function (jqXHR,exception){
            if (jqXHR.status===500){
                $.blockUI({ 
                    message: '<h3>Our server is experiencing issue</h3>', 
                    timeout: 2000 
                }); 
            }
        }
    })
})



function sendLikeData(id){
    socket.emit('likeAdd',{
        likeBy:getData('user'),
        likeOn:id
    },function(data){
        if (data==="Liked"){
            $(`#${id} #${id}likLog`).addClass('w3-text-black');
            $.ajax({
                url:api+'likeUpd',
                method:'post',
                data:{id:id},
                success:function(data){
                    $(`#${id}likSpan`).remove();
                    $(`#${id}`).append(`<span class="w3-badge likeSpan" style="margin-left:10px;" id="${id}likSpan">${data.length}</span>`);
                },
                error:function(jqXHR,exception){
                    if(jqXHR.status===500){
                        $.blockUI({ 
                            message: '<h3>Our server is experiencing issue</h3>', 
                            timeout: 2000 
                        });
                    }
                }
            })
        }else{
            $(`#${id} #${id}likLog`).removeClass('w3-text-black');
            $.ajax({
                url:api+'likeUpd',
                method:'post',
                data:{id:id},
                success:function(data){
                    $(`#${id}likSpan`).remove();
                    $(`#${id}`).append(`<span class="w3-badge likeSpan" style="margin-left:10px;" id="${id}likSpan">${data.length}</span>`);
                },
                error:function(jqXHR,exception){
                    if(jqXHR.status===500){
                        $.blockUI({ 
                            message: '<h3>Our server is experiencing issue</h3>', 
                            timeout: 2000 
                        });
                    }
                }
            })
        }
    })
}


function addMessage() {
    //this function will be used whenever a new user
    //adds a message 
    if ($('#textEnter').val()===""){
        $.blockUI({ 
            message: '<h3>Please Enter A Valid message</h3>', 
            timeout: 2000 
        }); 
    }else{
    socket.emit('createMessage', {
        from: 'User',
        text: jQuery('#textEnter').val()
    }, function(data) {
        $('#textEnter').val("");
    });
}
};

function sendCommData(id){
    $.ajax({
        url:api+'commFet',
        method:'post',
        data:{id:id},
        success:function(data){
            $('#comInp').empty()
            $('.cont').empty();
            $('.h').empty()
            for(i=0;i<data.length;i++){
                $('.cont').append(`<p class="w3-margin-top">${data[i].name} (${data[i].job}) : ${data[i].cont}<p>
                                    <br>
                                    <hr>`);
            }
            $('#comInp').append(`<input id="${id}" type="text" placeholder="Enter a comment" class="w3-input w3-margin-bottom">
            <script id="h">
            document.querySelector('#comInp .w3-input').addEventListener("keyup",function(event){
                if (event.keyCode===13){
                    comSockets('${id}');
                }
            })            
            </script>`);
            document.getElementById('id01').style.display='block';
        },
        //                    <input id="" type="text" placeholder="Enter a comment" class="w3-input w3-margin-bottom">

        error:function(jqXHR,exception){
            if(jqXHR.status===500){
                $.blockUI({ 
                    message: '<h3>Our server is experiencing issue</h3>', 
                    timeout: 2000 
                });  
            }
        }
    })
    // document.getElementById('id01').style.display='block';
}

function comSockets(id){
    var cont = $('#comInp .w3-input').val();
    var name = getData('user')[2].name;
    var job = getData('user')[2].job;
    var email = getData('user')[2].email;
    socket.emit('commAdd',{
        id:id,
        comBy:name,
        comJob:job,
        com:cont,
        email:email

    },function(data){
        $('.cont').append(`<p class="w3-margin-top">${name} (${job}) : ${cont}<p>
        <br>
        <hr>`)
        $.ajax({
            url:api+'comUpd',
            method:'post',
            data:{id:id},
            success:function(data){
                $('#comInp .w3-input').val('');
                $(`#${id}comSpan`).remove();
                $(`button#${id}.comm`).append(`<span class="w3-badge commSpan" style="margin-left:10px;" id="${id}comSpan">${data.length}</span>`);
            },
            error:function(jqXHR,exception){
                if(jqXHR.status===500){
                    $.blockUI({ 
                        message: '<h3>Our server is experiencing issue</h3>', 
                        timeout: 2000 
                    });  
                }
            }
        })
    })
}


function joinRoom(id){
    alert(id);
    socket.emit('joinRoom',{id:id},(data)=>{
        data = {id:id};
        $.ajax({
            url:api+'messFet1',
            method:'post',
            data:data,
            success:function(data){
                document.querySelector('.cbody').style.display="block";
                for(var i=0;i<data.length;i++){
                $('.chat-message').append(`
                <div class="chat-message-content clearfix w3-margin-bottom">
                            
                <span class="chat-time">${moment(data[i].createdAt).format('h:mm a')}</span>

                <h4>${data[i].messageFromName}</h4>

                <p>${data[i].messageValue}</p>
                <hr>
            </div>
                `)
                }
            }
        })
    })
}

function appendNotif(data){
    var countN=0;
        for(var j=0;j<data.length;j++){
                $('#messNotif').append(`<div class="w3-bar-item w3-button">${data[j].notifBy} : Messaged You</div>
                <hr>
                `)
                console.log(countN);
                if(data[j].status===0){
                    countN++;
                }
        }
        $('#messNotifBadge').remove();
        $('button.messNotif1').append(`<span class="w3-badge w3-right w3-small w3-green" id="messNotifBadge">${countN}</span>`)
}

