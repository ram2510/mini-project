var socket = io();

$('document').ready(function(){
    $.blockUI({ css: { 
        border: 'none', 
        padding: '15px', 
        backgroundColor: '#000', 
        '-webkit-border-radius': '10px', 
        '-moz-border-radius': '10px', 
        opacity: .5, 
        color: '#fff' 
    } }); 
    setTimeout($.unblockUI, 1000); 
    var user = getData('user');
    if(user===undefined){
        Nav.replace('/');
    }
    if(getData('fri')==undefined){
        Nav.replace('/view/main.html');
    }
    data = {
        email:getData('fri').email
    }
    $.ajax({
        url:api+"postFet",
        method:"post",
        data:data,
        success:function(data){
            if (data.length==0){
                $('#post').append(`<div class="w3-container w3-card w3-white w3-round w3-margin"><h3 class="w3-center">${getData('fri').name}'s Hasn't Posted yet!</h3></div>`)
            }else{
            for (i=0;i<data.length;i++){
                $('#post').append(`<div class="w3-container w3-card w3-white w3-hover-shadow w3-round w3-margin"><br>
                <!-- <img src="/w3images/avatar5.png" alt="Avatar" class="w3-left w3-circle w3-margin-right" style="width:60px"> -->
                <span class="w3-right w3-opacity">${moment(data[i].time).format("dddd, MMMM Do YYYY, h:mm a")}</span>
                <h4>${data[i].postName}</h4><br>
                <hr class="w3-clear">
                <div class="w3-padding">
${data[i].postCont}                   </div>
               <div class="w3-padding">
                   Language:${data[i].postLang}
               </div>
               <textarea id="codeArea_${i}" name="content" cols="80">
${data[i].postCode}        
       </textarea>   
            <div id="lik" class="lik">                                    
               <button type="button" class="w3-button w3-theme-d1 w3-margin-bottom like" onclick="sendLikeData(this.id)" id="${data[i]._id}"><i class="fa fa-thumbs-up" id="${data[i]._id}likLog"></i>  Like <span class="w3-badge likeSpan" style="margin-left:10px;" id="${data[i]._id}likSpan">${data[i].likes.length}</span></button>
               <button type="button" class="w3-button w3-theme-d2 w3-margin-bottom comm" onclick="sendCommData(this.id)" id="${data[i]._id}"><i class="fa fa-comment" id="${data[i]._id}comLog"></i>  Comment <span class="w3-badge commSpan" style="margin-left:10px;" id="${data[i]._id}comSpan">${data[i].comments.length}</span></button>
            </div>
        </div>
        
`)
for(j=0;j<data[i].likes.length;j++){
    // console.log(data[i].likes[j])
    // console.log(j);
    // console.log(getData('user')[2].name)
    if(getData('user')[2].name===data[i].likes[j]){
        // console.log(data[i].likes[j]);
        $(`#${data[i]._id} #${data[i]._id}likLog`).addClass('w3-text-black');
    }
}
            
}
            }
            for (var i=0;i<data.length;i++){
                $('#post').append(`<script id="codeEdit" language="javascript" type="text/javascript">
                editAreaLoader.init({
                    id : "codeArea_${i}"		// textarea id
                    ,syntax: "${data[i].postLang}"			// syntax to be uses for highgliting
                    ,allow_toggle:false
                    ,is_editable:false
                    ,start_highlight: true
                });		
                </script>`)
            }
            
        },
        error(jqXHR,exception){
            if (jqXHR.status===500){
                $.blockUI({ 
                    message: '<h3>Our server is experiencing issue</h3>', 
                    timeout: 2000 
                }); 
            }
        }
    })
    $('#view').append(prof(getData('fri')))
})



function sendLikeData(id){
    socket.emit('likeAdd',{
        likeBy:getData('user'),
        likeOn:id
    },function(data){
        if (data==="Liked"){
            $(`#${id} #${id}likLog`).addClass('w3-text-black');
            $.ajax({
                url:api+'likeUpd',
                method:'post',
                data:{id:id},
                success:function(data){
                    $(`#${id}likSpan`).remove();
                    $(`#${id}`).append(`<span class="w3-badge likeSpan" style="margin-left:10px;" id="${id}likSpan">${data.length}</span>`);
                },
                error:function(jqXHR,exception){
                    if(jqXHR.status===500){
                        $.blockUI({ 
                            message: '<h3>Our server is experiencing issue</h3>', 
                            timeout: 2000 
                        });
                    }
                }
            })
        }else{
            $(`#${id} #${id}likLog`).removeClass('w3-text-black');
            $.ajax({
                url:api+'likeUpd',
                method:'post',
                data:{id:id},
                success:function(data){
                    $(`#${id}likSpan`).remove();
                    $(`#${id}`).append(`<span class="w3-badge likeSpan" style="margin-left:10px;" id="${id}likSpan">${data.length}</span>`);
                },
                error:function(jqXHR,exception){
                    if(jqXHR.status===500){
                        $.blockUI({ 
                            message: '<h3>Our server is experiencing issue</h3>', 
                            timeout: 2000 
                        });
                    }
                }
            })
        }
    })
}


function sendCommData(id){
    $.ajax({
        url:api+'commFet',
        method:'post',
        data:{id:id},
        success:function(data){
            // console.log($('.cont').html())
            $('#comInp').empty()
            $('.cont').empty();
            $('.h').empty()
            // console.log($('.cont').html())
            for(i=0;i<data.length;i++){
                $('.cont').append(`<p class="w3-margin-top">${data[i].name} (${data[i].job}) : ${data[i].cont}<p>
                                    <br>
                                    <hr>`);
            }
            $('#comInp').append(`<input id="${id}" type="text" placeholder="Enter a comment" class="w3-input w3-margin-bottom">
            <script id="h">
            document.querySelector('#comInp .w3-input').addEventListener("keyup",function(event){
                if (event.keyCode===13){
                    comSockets('${id}');
                }
            })            
            </script>`);
            document.getElementById('id01').style.display='block';
        },
        //                    <input id="" type="text" placeholder="Enter a comment" class="w3-input w3-margin-bottom">

        error:function(jqXHR,exception){
            if(jqXHR.status===500){
                $.blockUI({ 
                    message: '<h3>Our server is experiencing issue</h3>', 
                    timeout: 2000 
                });  
            }
        }
    })
    // document.getElementById('id01').style.display='block';
}

function comSockets(id){
    var cont = $('#comInp .w3-input').val();
    var name = getData('user')[2].name;
    var job = getData('user')[2].job;
    socket.emit('commAdd',{
        id:id,
        comBy:name,
        comJob:job,
        com:cont,

    },function(data){
        console.log(data);
        $('.cont').append(`<p class="w3-margin-top">${name} (${job}) : ${cont}<p>
        <br>
        <hr>`)
        $.ajax({
            url:api+'comUpd',
            method:'post',
            data:{id:id},
            success:function(data){
                $('#comInp .w3-input').val('');
                $(`#${id}comSpan`).remove();
                $(`button#${id}.comm`).append(`<span class="w3-badge commSpan" style="margin-left:10px;" id="${id}comSpan">${data.length}</span>`);
            },
            error:function(jqXHR,exception){
                if(jqXHR.status===500){
                    $.blockUI({ 
                        message: '<h3>Our server is experiencing issue</h3>', 
                        timeout: 2000 
                    });  
                }
            }
        })
    })
}

socket.on('newMessage',(data)=>{
    console.log(data);
    $('.chat-message').append(`
            <div class="chat-message-content clearfix w3-margin-bottom">
						
            <span class="chat-time">${moment(data.createdAt).format('h:mm a')}</span>

            <h4>${data.messageFrom}</h4>

            <p>${data.messageValue}</p>
            <hr>
        </div>
            `)
})