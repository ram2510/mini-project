//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

const postRoutes = require('./post_routes');

module.exports = function (app,db){
    postRoutes(app,db);
}