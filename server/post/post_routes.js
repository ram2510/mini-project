//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved
const moment = require('moment');


module.exports = function(app,db){
    app.post('/postAdd',(req,res)=>{
        var post = {
            postBy : req.body.email,
            postName:req.body.name,
            postCode : req.body.code,
            postLang:req.body.lang,
            postCont:req.body.cont,
            likes:[],
            comments:[],
            time:moment().valueOf(),
        }
        db.collection('posts').insertOne(post,(err,result)=>{
            if (err){
                res.status(500).send([500,"Internal Server error"]);
            }else{
                db.collection('users').find({email:req.body.email}).toArray((err,result)=>{
                    if(err){
                        res.status(500).send([500,"Internal Server error"]);
                    }else{
                        var postNo=result[0].noOfPosts;
                        postNo+=1;
                        var post = {$set:{noOfPosts:postNo}}
                        db.collection('users').updateOne({email:req.body.email},post,(err,result)=>{
                            if(err){
                                res.status(500).send([500,"Internal Server Error"]);
                            }else{
                                res.status(200).send([200,"Ok"]);
                            }
                        })
                    }
                })
            }
        });
    });
    app.post('/postDel',(req,res)=>{
        var email = req.body.email
        db.collection('posts').deleteOne({postBy:email},(err,result)=>{
            if (err){
                res.status(500).send([500,"Internal Server error"]);
            }else{
                res.status(200).send([200,"Ok"]);
            }
        });
    })
    app.post('/postFet',(req,res)=>{
        var posts = {
            postBy:req.body.email
        }
        db.collection('posts').find(posts).toArray(function(err,result){
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                res.status(200).send(result);
            }
        })
    })
    app.post('/post',(req,res)=>{
        db.collection('posts').find().toArray(function(err,result){
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                res.status(200).send(result);
            }
        })
    })

}