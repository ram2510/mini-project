//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

const ObjectId = require('mongodb').ObjectID;

module.exports=function(app,db){
    app.post('/likeUpd',(req,res)=>{
        var id=req.body.id;
        var id = new ObjectId(id);
        db.collection('posts').find({_id:id}).toArray(function(err,result){
            if(err){
                res.status(500).send([500,"Internal Server error"]);
            }else{
                res.send(result[0].likes);
            }

        })
    })
    app.post('/commFet',(req,res)=>{
        var id=req.body.id;
        var id = new ObjectId(id);
        db.collection('posts').find({_id:id}).toArray(function(err,result){
            if(err){
                res.status(500).send([500,"Internal Server error"]);
            }else{
                console.log(result[0]);
                res.send(result[0].comments);
            }

        })
    })

    app.post('/comUpd',(req,res)=>{
        var id = req.body.id;
        var id = new ObjectId(id);
        db.collection('posts').find({_id:id}).toArray(function(err,result){
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                res.send(result[0].comments);
            }
        })
    })

    app.post('/notifFet',(req,res)=>{
        var email = req.body.email;
        var details={notifOnName:email}
        db.collection('notification').find(details).toArray(function(err,result){
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                res.status(200).send(result);
                db.collection('notification').updateMany(details,{$set:{status:1}},(err,result)=>{
                    if (err){
                        console.log(err);
                    }
                })
            }
        })
    })

    app.post('/messFet',(req,res)=>{
        var person1 = req.body.messageFromEmail
        var person2 = req.body.messageToEmail;
        var query = {$or:[{
            person1:req.body.messageToEmail,
            person2:req.body.messageFromEmail
        },
        {
            person1:req.body.messageFromEmail,
            person2:req.body.messageToEmail
        }
    ]}
        db.collection('messages').find(query).toArray((err,result)=>{
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                
                if(result.length===0){
                    res.status(200).send(null);
                }else{
                res.status(200).send(result[0].messages);
                }

            }
        })
    })
    app.post('/messNot',(req,res)=>{
        var name = req.body.userName;
        var query = {$and:[{type:"message"},{notifOn:name}]};
        db.collection('notification').find(query).toArray((err,result)=>{
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                res.status(200).send(result);
                db.collection('notification').updateMany(query,{$set:{status:1}},(err,result)=>{
                    if (err){
                        console.log(err);
                    }
                })
            }
        })
    })
}