//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

//we require the third party modules here
var crypto = require('crypto');

function generateSalt(length){
    //this function will be used to generate to
    //store passwords and make it more secure
    return crypto.randomBytes(Math.ceil(length/2))
    .toString('hex') /** convert to hexadecimal format */
    .slice(0,length);   /** return required number of characters */
};

var sha256 = function(password, salt){
    //this function will be used to create passwords
    var hash = crypto.createHmac('sha256', salt); /** Hashing algorithm sha512 */
    hash.update(password);
    var value = hash.digest('hex');
    return value;
};


module.exports = {
    generateSalt,
    sha256,
}