//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

//we require the third party modules here
var path = require('path');
var express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const bp = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;    
const moment = require('moment');   
const {generateSalt} = require('./functions');

//we require the file with db
const db = require('../config/db');

//we require the modules i created
const { generateMessage, generateLocationMessage } = require('./utils/message');
// const {generateSalt,sha256} = require('./functions');

//we set the port
const port = process.env.PORT || 3000;

//initialised express
var app = express();
var server = http.createServer(app);
var io = socketIO(server);

//support json parsing of json data
app.use(bp.json());
//support json parsing of x-www-form-urlencoded data
app.use(bp.urlencoded({extended:true}));

var room;

//we create the directory
const publicPath = path.join(__dirname, '../public');
app.use(express.static(publicPath));


io.on('connection', (socket) => { //here socket represents a single instance
    socket.on('likeAdd',function(data,callback){
        MongoClient.connect(db.url, {useNewUrlParser: true },(err,database)=>{
            if(err) return console.log(err);
            database = database.db("socialNetwork");
            var id1 =data.likeOn;
            id = new ObjectId(id1);
            // console.log(data.likeBy[2]);
            var id = {_id:id}
            var like = { $push: { likes : data.likeBy[2].name } }
            var unlike = {$pull:{likes : data.likeBy[2].name}}
            
            database.collection('posts').findOne(id,(err,result)=>{
                if(err){
                    callback([500,err]);
                }else{
                    var notif= {
                        type:"like",
                        notifBy:data.likeBy[2].name,
                        notifByJob:data.likeBy[2].job,
                        notifOn:result.postBy,
                        notifAt:moment().valueOf(),
                        id:id1,
                        notifOnName:result.postName,
                        status:0
                    }
                    var notifDel = {
                        id:id1,
                        notifBy:data.likeBy[2].name,
                        notifOn:result.postBy,
                        type:"like",
                        notifOnName:result.postName
                    }
                    var match = false;
                    // console.log(result.likes)
                    // callback(result.likes);
                    var likes = result.likes;
                    for (i=0;i<likes.length;i++){
                        if(likes[i]===data.likeBy[2].name){
                            match = true;
                        }
                    }
                    if(!match){
                        database.collection('posts').updateOne(id,like,(err,result)=>{
                            if(err){
                                callback([500,err]);
                            }else{
                                database.collection('notification').insertOne(notif,(err,result)=>{
                                    if (err){
                                        callback([500,err]);
                                    }else{
                                        callback('Liked');

                                    }
                                })
                            }
                        })
                    }else{
                        database.collection('posts').updateOne(id,unlike,(err,result)=>{
                            if(err){
                                callback([500,err]);
                            }else{
                                
                                database.collection('notification').deleteOne(notifDel,(err,result)=>{
                                    if(err){
                                        callback([500,err]);
                                    }else{
                                        callback('Unliked')
                                    }
                                })
                            }
                        })
                    }

                }
            })        
        })
    })

    socket.on('commAdd',function(data,callback){
        MongoClient.connect(db.url, {useNewUrlParser: true },(err,database)=>{
            if(err) return console.log(err);

            database = database.db("socialNetwork");
            var id1 = data.id;
            var cont= data.com;
            var name = data.comBy;
            var job = data.comJob;
            id  = new ObjectId(id1);
            var id = {_id:id};
            var comm = {$push:{comments:{name:name,
                                        job:job,
                                        cont:cont
                                    }}}
            var notifAdd = {
                type:"comment",
                notifBy:name,
                notifByJob:job,
                notifOn:"",
                notifAt:moment().valueOf(),
                id:id1,
                notifOnName:"",
                status:0
            }
            database.collection('posts').findOne(id,(err,result)=>{
                notifAdd.notifOn=result.postName;
                notifAdd.notifOnName = result.postBy;
            })        
            database.collection('posts').updateOne(id,comm,(err,result)=>{
                if (err){
                    callback([500,err]);
                }else{
                    database.collection('notification').insertOne(notifAdd,(err,result)=>{
                        if(err){
                            callback([500,err]);
                        }else{
                            callback("Added")
                        }
                    })
                }
            })

        })
    })

    socket.on('room',(data,callback)=>{
        // console.log('1 h');
        MongoClient.connect(db.url,{ useNewUrlParser: true },(err,database)=>{
            if (err) return console.log(err);
            // console.log('2');
            database = database.db("socialNetwork");
            var query = {$or:[{
                person1:data.messageToEmail,
                person2:data.messageFromEmail
            },
            {
                person1:data.messageFromEmail,
                person2:data.messageToEmail
            }
        ]}
            database.collection('messages').findOne(query,(err,result)=>{
                // console.log('3');
                if (result===null){
                     room=generateSalt(6);
                    socket.join(room);
                    io.in(room).clients(function(error,clients){
                        var numClients=clients.length;
                        console.log(numClients);                           
                    }); 
                    var messDoc = {
                        roomId:room,
                        person1:data.messageToEmail,
                        person2:data.messageFromEmail,
                        person1Name:data.messageToName,
                        person2Name:data.messageFromName,
                        messages:[],
                    }
                    database.collection('messages').insertOne(messDoc,(err,result)=>{
                        if(err){
                            console.log(err);
                            callback(false);
                        }else{
                            // console.log('5');
                            callback(true);
                        }
                    })
                }else{
                    // console.log('6');
                    room =result.roomId;
                    socket.join(room);
                    io.in(room).clients(function(error,clients){
                        var numClients=clients.length;
                        console.log(numClients);                           
                    }); 
                }
            })
        })
    })

    socket.on('joinRoom',(data,callback)=>{
        MongoClient.connect(db.url,{useNewUrlParser:true},(err,database)=>{
            if(err) return console.log(err);
            database = database.db("socialNetwork");

            var query={roomId:data.id}
            socket.join(room);
            io.in(room).clients(function(error,clients){
                var numClients=clients.length;
                console.log(numClients);                           
            }); 
            callback(true);
        })
    })

    socket.on('sendMessage',(data,callback)=>{
        MongoClient.connect(db.url,{useNewUrlParser:true},(err,database)=>{
            if(err) return console.log(err);
            
            database = database.db("socialNetwork");
            var query = {$or:[{
                person1:data.messageToEmail,
                person2:data.messageFromEmail
            },
            {
                person1:data.messageFromEmail,
                person2:data.messageToEmail
            }
        ]}
        database.collection('messages').findOne(query,(err,result)=>{
            var roomId = result.roomId;
            var message = {$push:{messages:data}}
            var details = {roomId:roomId}
            database.collection('messages').updateOne(details,message,(err,result)=>{
                if(err){
                    callback(false);
                }else{
                    var notifAdd = {
                        type:"message",
                        notifBy:data.messageFromEmail,
                        notifOn:data.messageToEmail,
                        notifAt:moment().valueOf(),
                        notifOnName:data.messageToName,
                        status:0,
                    }
                    database.collection('notification').insertOne(notifAdd,(err,result)=>{
                        if(err){
                            callback([500,err]);
                        }else{
                            io.in(roomId).emit("newMessage",{
                                messageFrom:data.messageFromName,
                                messageTo:data.messageToEmail,
                                createdAt:moment().valueOf(),
                                messageValue:data.messageValue,
                                id:roomId,
                            })
                            io.in(roomId).clients(function(error,clients){
                                var numClients=clients.length;
                                console.log(numClients);                           
                            }); 
                            console.log("hello");
                            callback(true);
                        }
                    })
                }
            })
        })
        })
    })

})


//we connect to the database here
MongoClient.connect(db.url,{ useNewUrlParser: true },(err,database)=>{
    if(err) return console.log(err);

    database = database.db("socialNetwork");
    //the user part
    //we require the user function
    require('./user/index')(app,database);
    //the friends part
    //we require the friends function
    require('./friends/index')(app,database);
    //the post part
    //we require the post function
    require('./post/index')(app,database);
    //the likeComment part
    //we require the likeComment function
    require('./likComm/index')(app,database);
});


//we listen to the port here 
server.listen(port, () => {
    console.log(`Server started on port ${port}`);
})