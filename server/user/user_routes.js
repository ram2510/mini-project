//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

//we reuire the ObjectId here
const validator = require('validator');
//we require the modules I created
const {generateSalt,sha256} = require("../functions");

module.exports = function(app,db){
    //here we will create a new user
    app.post('/user',(req,res)=>{
        // console.log(req.body.name);
            var email = req.body.email;
            if (validator.isEmail(email)){
            const details = {'email':email};
            db.collection('users').findOne(details,(err,result)=>{
                if(err){
                    res.status(500).send(err);
                }
                if(result==null){
                    var salt=generateSalt(6);
                    var pass = sha256(req.body.password,salt);
                    const user ={
                            name:req.body.name,
                            job:req.body.job,
                            email:req.body.email,
                            password:pass,
                            salt:salt,
                            hash_algo:"sha256",
                            CB:0,
                            noOfPosts:0,
                        };
                        //we will insert it into the users collection
                        db.collection('users').insertOne(user,(err,result)=>{
                            if (err){
                                res.status(500).send(err);
                            }else{
                                res.status(200).send([200,"Ok"]);
                            }
                        })

                }else{
                    res.status(401).send([401,"Unauthosied"]);
                }
            })
        }else{
            res.status(400).send([400,"Bad Request"]);
        }
    });

    //here we will fetch the user
    app.post('/userFet',(req,res)=>{
        var email = req.body.email;
        var pass = req.body.password
        const details = {'email':email};
        db.collection('users').findOne(details,(err,result)=>{
            if(err){
                res.status(500).send(err);
            }else{
                try
                {var salt = result.salt;
                console.log(salt);
                console.log(sha256(pass,salt));
                console.log(result.password);
                if(sha256(pass,salt)===result.password){
                    res.send([200,"Ok",result]);
                }else{
                    res.status(401).send([401,"Unauthorised"]);
                }
            }catch(err){
                res.status(500).send([500,"Internal Server Error"]);
            }
            }
        
        })
        // res.send(email);
    });
    app.post('/userFetRef',(req,res)=>{
        var email = req.body.email;
        const details = {'email':email};
        db.collection('users').findOne(details,(err,result)=>{
            if(err){
                res.status(500).send(err);
            }else{
                res.send([200,"Ok",result]);
            }
        
        })
        // res.send(email);
    });
    //here we will delete the user
    app.post('/userCFet',(req,res)=>{
        const email = req.body.email;
        db.collection('users').findOne({email:email});
    });
    //here we will update the user
    app.post('/userUpd',(req,res)=>{
        const email = req.body.email;
        const details = {'email':email};
        db.collection('users').findOne(details,(err,result)=>{
            if(err){
                res.status(500).send(err);
            }
            if(result==null){
                const user = {name:req.body.name,};
                db.collection('users').update(details,user,(err,result)=>{
                if (err){
                    res.send(err);
                }else{
                    res.send(result);
            }
        })
            }
        });
        
    })
    app.post('/userAll',(req,res)=>{
        db.collection('users').find({}).toArray((err,result)=>{
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }else{
                res.status(200).send(result);
            }
        })
    })
}