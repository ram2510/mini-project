//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

//we require the userRoutes function
const friendRoute= require('./friends_routes');

module.exports=function(app,db){
    friendRoute(app,db);
}