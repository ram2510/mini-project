//Author : Ram Pandey
//Copyright 2019, Ram Pandey , All rights Reserved

module.exports = function(app,db){
    app.post('/friAdd',(req,res)=>{
        const friPair = {
            fri1:req.body.fri1,
            fri2:req.body.fri2,
            job2:req.body.job2,
        }
        db.collection('friends').insertOne(friPair,(err,result)=>{
            if(err){
                res.status(500).send([500,"Internal server error"]);
            }
            else{
                res.status(200).send([200,"Ok"]);
            }
        });
    });
    app.post('/friDel',(req,res)=>{
        const friPair = {
            fri1:req.body.fri1,
            fri2:req.body.fri2,
        }
        db.collection('friends').deleteOne(friPair,(err,result)=>{
            if(err){
                res.status(500).send([500,"Internal Server Error"]);
            }
            else{
            res.status(200).send([200,"Ok"]);
            }
        });
    })
    
}